import csv
# encoding=utf8
import sys

reload(sys)
sys.setdefaultencoding('utf8')
if sys.version_info[0] < 3:
    import got
else:
    import got3 as got


def main():
    criteria = got.manager.TweetCriteria().setQuerySearch('#testtweet').setSince("2015-05-01").setUntil(
        "2015-09-30")
    with open('output.csv', 'a') as f:
        writer = csv.writer(f, delimiter=",")
        for t in got.manager.TweetManager.getTweets(criteria):
            writer.writerow([t.username, t.retweets, t.text, t.mentions, t.hashtags])


if __name__ == '__main__':
    main()
